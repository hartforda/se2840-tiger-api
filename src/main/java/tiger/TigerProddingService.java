package main.java.tiger;

import org.json.JSONException;
import org.json.JSONObject;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import java.util.Random;

/**
 * TigerProddingService.java
 * API that allows you all the fun of screwing with a tiger, without the risk!  Hope you're lucky!
 * Created by hartforda on 1/30/2017.
 */
@Path("/tigerprodder")
public class TigerProddingService {
        @GET
        @Produces("application/json")
        public Response pokeTiger() throws JSONException {

            JSONObject jsonObject = new JSONObject();

            String userAction = "You poked the tiger with a stick!";

            int min = 1;
            int max = 100;
            Random random = new Random();
            int r = min + random.nextInt((max - min) + 1);

            String tigerAction = "";
            if (r < 40) tigerAction = "Tiger is charging!";
            if (r > 40 && r < 80) tigerAction = "Tiger growls... better not try that again!";
            if (r > 80) tigerAction = "Tiger doesn't acknowledge you... Phew!";

            jsonObject.put("RandGenerated", r);
            jsonObject.put("UserAction", userAction);
            jsonObject.put("TigerReply", tigerAction);

            //String result = "@Produces(\"application/json\") Output: \n\nTiger Prodding Service Output: \n\n" + jsonObject;
            String result = "" + jsonObject;
            return Response.status(200).entity(result).build();
        }

        @Path("{action}")
        @GET
        @Produces("application/json")
        public Response actionTiger(@PathParam("action") String action) throws JSONException {

            JSONObject jsonObject = new JSONObject();

            String userAction = action;

            Random random = new Random();
            int min = 1;
            int max = 100;
            int r = min + random.nextInt((max - min) + 1);

            String tigerAction = "";
            if (action.equals("poke")) {
                userAction = "You poked the tiger with a stick!";
                if (r < 40) tigerAction = "Tiger is charging!";
                if (r > 40 && r < 80) tigerAction = "Tiger growls... better not try that again!";
                if (r > 80) tigerAction = "Tiger doesn't acknowledge you... Phew!";
            }

            else if (action.equals("rock")) {
                userAction = "You threw a rock at the tiger!";
                if (r < 60) tigerAction = "Tiger is charging!";
                if (r > 60 && r < 90) tigerAction = "Tiger growls... better not try that again!";
                if (r > 90) tigerAction = "Tiger doesn't acknowledge you... Phew!";

            } else if (action.equals("yell")) {
                userAction = "You yelled at the tiger!";
                if (r < 20) tigerAction = "Tiger is charging!";
                if (r > 20 && r < 50) tigerAction = "Tiger growls... better not try that again!";
                if (r > 50) tigerAction = "Tiger doesn't acknowledge you... Phew!";

            } else {
                userAction = "You did nothing... your friends mock you for being such a pansy!";
                if (r < 40) tigerAction = "Tiger is charging!";
                if (r > 40 && r < 80) tigerAction = "Tiger growls... better not try that again!";
                if (r > 80) tigerAction = "Tiger doesn't acknowledge you... Phew!";
            }

            jsonObject.put("RandGenerated", r);
            jsonObject.put("UserAction", userAction);
            jsonObject.put("TigerReply", tigerAction);

            //String result = "@Produces(\"application/json\") Output: \n\nTiger Prodding Service Output: \n\n" + jsonObject;
            String result = "" + jsonObject;
            return Response.status(200).entity(result).build();
        }
}
